//
//  ViewController.swift
//  Final
//
//  Created by Dongxuan Ji on 12/14/19.
//  Copyright © 2019 Dongxuan Ji. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    //MARK: Declare Buttons
    
    @IBOutlet weak var redBtn: UIButton! // red button
    
    @IBOutlet weak var blueBtn: UIButton! // blue button
    
    @IBOutlet weak var infoLabel: UILabel! // information Lable: show the instruction informations about the background color
    
    @IBOutlet weak var colorSwitch: UISwitch! // color switch: users can switch between the blue and the red selections
    
    //MARK: Button Action Functions
    
    //Event response when click the red button
    
    @IBAction func redBtnAction(_sender: Any) {
        
        self.view.backgroundColor = .red //When click red button, change the background color to red
        infoLabel.text = "Red was selected"
        colorSwitch.isOn = false // Color toggle is off, refers to Red
        
         }
    
    //Event response when click the blue button
    
    @IBAction func blueBtnAction(_sender: Any) {
        
        self.view.backgroundColor = .blue //When click blue button, change the background color to blue
        infoLabel.text = "Blue was selected"
        colorSwitch.isOn = true // Color toggle is on, refers to Blue
        
    }
    
    //Event response when click the color toggle
    
    @IBAction func colorSwitchAction(_sender: Any) {
        // When the toggle is on, change the main view background color to blue;  when the toggle is on, change the main view background color to red
        if colorSwitch.isOn == true {
            self.view.backgroundColor = .blue
            infoLabel.text = "Blue was selected"
        } else {
            self.view.backgroundColor = .red
            infoLabel.text = "Red was selected"
        }
    }
    
    //MARK: App Original View
    //Settings of original view when open the app
    
    override func viewDidLoad() {
        self.view.backgroundColor = .darkGray // Set the original main view color
        infoLabel.textAlignment = NSTextAlignment.center // Make the information label text to the center of the label
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

